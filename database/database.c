#include <stdio.h>
#include <strings.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <ftw.h>
#define PORT 8080



int main(int argc, char const *argv[]) {
int server_fd, new_socket, valread,b;
    char path[300] = "/databases/";
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    //set socket
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
    printf("Socket successfully created\n");

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    printf("Socket successfully set\n");

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
    if(bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0){
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    printf("Socket successfully binded\n");

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    printf("Socket successfully listening\n");

    if ((new_socket = accept(server_fd, (struct sockaddr *)NULL, NULL))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }
    printf("Socket successfully accepted\n");
    valread = read( new_socket , buffer, 1024);
    while (1)
    {
        //if buffer is use 
        if(valread == 0){
            printf("Server disconnected\n");
            break;
        }
        char *delimiter = " ;,()intstringdatechartext";
        char *token = strtok(buffer, delimiter);
        if(strcmp(token, "CREATE") == 0){
            token = strtok(NULL, " ");
            if(strcmp(token, "DATABASE") == 0){
                token = strtok(NULL, delimiter);
                char databasepath[200];
                sprintf(databasepath, "/databases/%s",token);
                if(access(databasepath, F_OK) != -1){
                    printf("database already exist\n");
                    valread = read( new_socket , buffer, 1024);
                    continue;
                }
                create_database(databasepath);
            }
            else if(strcmp(token, "TABLE") == 0){
                token = strtok(NULL, " ");
                char tablepath[200];
                sprintf(tablepath, "%s%s",path,token);
                mkdir(tablepath, 0777);
                while(token != NULL){
                    token = strtok(NULL, delimiter);
                    char columnpath[400];
                    sprintf(columnpath, "%s/%s",tablepath,token);
                    if(token != NULL){
                        FILE *fp = fopen(columnpath, "w");
                        fclose(fp);
                    }
                }
                
            }
        }
        else if(strcmp(token, "USE") == 0){
            token = strtok(NULL, delimiter);
            strcat(path, token);
        }
    }
    
}
